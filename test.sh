#!/usr/bin/bash
export CCACHE_DISABLE=1

C_FLAGS=`pkg-config --libs --cflags gtk+-3.0`
CXX_FLAGS=`pkg-config --libs --cflags gtkmm-3.0`

cat hello.c
echo "C   lines            " `cat hello.c | wc -l`
cat hello.cpp
echo "C++ lines            " `cat hello.cpp | wc -l`
echo "C   lines (expanded) " `gcc $C_FLAGS hello.c -E | wc -l`
echo "C++ lines (expanded) " `g++ $CXX_FLAGS hello.cpp -E | wc -l`

hyperfine --warmup 2 --min-runs $RUNS "gcc $C_FLAGS hello.c -o hello_c" "g++ $CXX_FLAGS hello.c -o hello_cpp"
