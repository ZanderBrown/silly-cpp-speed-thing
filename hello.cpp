#include <gtkmm.h>

int
main (int argc, char **argv)
{
  Gtk::Main kit(argc, argv);

  auto window = Gtk::Window(Gtk::WINDOW_TOPLEVEL);
  window.show();

  Gtk::Main::run(window);

  return 0;
}
